# Basic Go API
A basic RESTful API using GoLand working as FaaS.

## Routes
* GET `api/v1/users`
* POST `api/v1/users`
* GET `api/v1/users/:id`
* PUT `api/v1/users/:id`
* DELETE `api/v1/users/:id`

## References

https://github.com/gorilla/mux

https://thenewstack.io/make-a-restful-json-api-go/

https://github.com/mlabouardy/movies-restapi