package main

type User struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Email string `json:"email"`
}

var users []User

func UserList() []User {
	return users
}

func UserShow(userId int) User {
	return UserById(userId)
}

func UserCreate(user User) User {
	users = append(users, user)
	return user
}

func UserUpdate(user User) User {
	var usersAux []User
	for _, u := range users {
		if int(u.ID) == int(user.ID) {
			usersAux = append(usersAux, user)
		}else{
			usersAux = append(usersAux, u)
		}
	}
	users = usersAux
	return user
}

func UserDelete(userId int) {
	var usersAux []User
	for _, user := range users {
		if int(user.ID) != int(userId) {
			usersAux = append(usersAux, user)
		}
	}
	users = usersAux
}

func UserById(userId int) User {
	user := User{}
	for _, u := range users {
		if int(u.ID) == int(userId) {
			user = u
		}
	}

	return user
}

func UserPopulate() {
	users = append(users,
		User {
			ID: 1,
			Name: "Rafael Mateus",
			Email: "rafaelmateus@hotmail.com.br",
		},
		User {
			ID: 2,
			Name: "Leopoldo",
			Email: "leopoldo@gmail.com",
		},
		User {
			ID: 3,
			Name: "Inácio",
			Email: "inacio@gmail.com",
		},
	)
}