package main

import (
	"log"
	"net/http"
)

func main() {
	// populate array of users
	UserPopulate()

	r := NewRouter()
	log.Fatal(http.ListenAndServe(":8080", r))
}
