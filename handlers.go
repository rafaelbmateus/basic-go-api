package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

func UserListHandler(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(UserList())
}

func UserShowHandler(w http.ResponseWriter, r *http.Request) {
	id := UserShow(getUserId(r))
	json.NewEncoder(w).Encode(id)
}

func UserCreateHandler(w http.ResponseWriter, r *http.Request) {
	id := len(UserList()) + 1
	name := r.URL.Query().Get("name")
	email := r.URL.Query().Get("email")
	user := User{id, name, email}
	json.NewEncoder(w).Encode(UserCreate(user))
}

func UserUpdateHandler(w http.ResponseWriter, r *http.Request) {
	id := getUserId(r)
	name := r.URL.Query().Get("name")
	email := r.URL.Query().Get("email")
	user := User{id, name, email}
	json.NewEncoder(w).Encode(UserUpdate(user))
}

func UserDeleteHandler(w http.ResponseWriter, r *http.Request) {
	id := getUserId(r)
	UserDelete(id)
}

func getUserId(r *http.Request) int {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		return 0
	}
	return id
}